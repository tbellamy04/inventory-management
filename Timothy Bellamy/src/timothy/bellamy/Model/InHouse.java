/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timothy.bellamy.Model;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
/**
 *
 * @author Timothy Bellamy
 */
public class InHouse extends Part {
    private final IntegerProperty _machineId = new SimpleIntegerProperty(0);

    public InHouse(int id, String name, double price, int stock, int min, int max, int machine) {
        super(id, name, price, stock, min, max);
        setMachineId(machine);
    }
    
    public int getMachineId() {
        return _machineId.get();
    }
    public final void setMachineId(int value) {
        _machineId.set(value);
    }
    public IntegerProperty _machineIdProperty() {
        return _machineId;
    }
}
