/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timothy.bellamy.Model;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Timothy Bellamy
 */
    public class Inventory {
    public ObservableList<Product> products;
    public ObservableList<Part> allParts;
    private int _uniquePartId;
    private int _uniqueProductId;

   public Inventory() {
        products = FXCollections.observableArrayList();
        allParts = FXCollections.observableArrayList();
       
        // Fills inventory with default parts and products 
        addPart(new InHouse((allParts.size()), "Part A", 5.50, 5, 1, 10, 1));
        addPart(new InHouse((allParts.size()), "Part B", 7.50, 15, 10, 35, 2));
        addPart(new InHouse((allParts.size()), "Part C", 10.50, 25, 10, 85, 3));
        addPart(new Outsourced((allParts.size()), "Part D", 15.50, 10, 5, 25, "Amazon"));
        addPart(new Outsourced((allParts.size()), "Part E", 10.75, 5, 0, 10, "Greyco"));
        
        ObservableList<Part> partListA = FXCollections.observableArrayList();
        partListA.add(allParts.get(0));
        partListA.add(allParts.get(1));
        partListA.add(allParts.get(2));
        
        ObservableList<Part> partListB = FXCollections.observableArrayList();
        partListB.add(allParts.get(3));
        partListB.add(allParts.get(4));
        
        addProduct(new Product((products.size()), "Product A", 85.65, 5, 0, 10, partListA));
        addProduct(new Product((products.size()), "Product B", 150.62, 2, 0, 15, partListB));
        _uniquePartId = allParts.size();
        _uniqueProductId = products.size();
    }

    public int getUniquePartId() {
        return _uniquePartId;
    }

    public void setUniquePartId(int _uniquePartId) {
        this._uniquePartId = _uniquePartId;
    }

    public int getUniqueProductId() {
        return _uniqueProductId;
    }

    public void setUniqueProductId(int _uniqueProductId) {
        this._uniqueProductId = _uniqueProductId;
    }
    
    // Product list operations
   public final void addProduct(Product newProduct){
        products.add(newProduct);
    }
   public boolean removeProduct(int id){
        for(Product _product : products){
            if(_product.getProductId() == id){
                return products.remove(_product);
            }
        }
        return false;
    }
   public Product lookupProduct(int id){
        for(Product _product : products){
            if(_product.getProductId() == id){
                return _product;
            }
        }
        return null;
    }
   public void updateProduct(int id){
        for(Product _product : products){
            if(_product.getProductId() == id){
                
            }
        }
    }
   
    // Part list operations
   public final void addPart(Part newPart){
       allParts.add(newPart);
   } 
    public boolean deletePart(int id){
       for(Part _part : allParts){
           if(_part.getPartId() == id){
              return allParts.remove(_part);
           }
       }
       return false;
   }
   public Part lookupPart(int id){
       for(Part _part : allParts){
           if(_part.getPartId() == id){
               return _part;
           }
       }
       return null;
   }
   void updatePart(int id){
       
   }
}
