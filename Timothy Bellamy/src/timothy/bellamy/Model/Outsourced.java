/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timothy.bellamy.Model;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Timothy Bellamy
 */
public class Outsourced extends Part {
    private final StringProperty _companyName = new SimpleStringProperty("");

    public Outsourced(int id, String name, double price, int stock, int min, int max, String company) {
        super(id, name, price, stock, min, max);
        setCompanyName(company);
    }
    
    public String getCompanyName() {
        return _companyName.get();
    }

    public final void setCompanyName(String value) {
        _companyName.set(value);
    }

    public StringProperty _companyNameProperty() {
        return _companyName;
    }
}
