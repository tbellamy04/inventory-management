/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timothy.bellamy.Model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Timothy Bellamy
 */
public abstract class Part {
    private final SimpleIntegerProperty _partId = new SimpleIntegerProperty(0);
    private final SimpleStringProperty _name = new SimpleStringProperty("");
    private final SimpleDoubleProperty _price = new SimpleDoubleProperty(0);
    private final SimpleIntegerProperty _inStock = new SimpleIntegerProperty(0);
    private final SimpleIntegerProperty _min = new SimpleIntegerProperty(0);
    private final SimpleIntegerProperty _max = new SimpleIntegerProperty(0);

    public Part(int id, String name, double price, int stock, 
                int min, int max) {
        setPartId(id);
        setName(name);
        setPrice(price);
        setInStock(stock);
        setMin(min);
        setMax(max);
    }
     
    public int getMax() {
        return _max.get();
    }
    public final void setMax(int value) {
        _max.set(value);
    }
    public IntegerProperty _maxProperty() {
        return _max;
    }
    public int getMin() {
        return _min.get();
    }
    public final void setMin(int value) {
        _min.set(value);
    }
    public IntegerProperty _minProperty() {
        return _min;
    }
    public int getInStock() {
        return _inStock.get();
    }
    public final void setInStock(int value) {
        _inStock.set(value);
    }
    public IntegerProperty _inStockProperty() {
        return _inStock;
    }
    public double getPrice() {
        return _price.get();
    }
    public final void setPrice(double value) {
        _price.set(value);
    }
    public DoubleProperty _priceProperty() {
        return _price;
    }  
    public String getName() {
        return _name.get();
    }
    public final void setName(String value) {
        _name.set(value);
    }
    public StringProperty _nameProperty() {
        return _name;
    }   
    public int getPartId() {
        return _partId.get();
    }
    public final void setPartId(int value) {
        _partId.set(value);
    }
    public IntegerProperty _partIdProperty() {
        return _partId;
    }
}
