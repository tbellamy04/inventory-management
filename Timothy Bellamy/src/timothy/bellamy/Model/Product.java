/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timothy.bellamy.Model;
import java.util.ArrayList;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Timothy Bellamy
 */
public class Product {
    private final IntegerProperty _productId = new SimpleIntegerProperty(0);
    private final StringProperty _name = new SimpleStringProperty("");
    private final DoubleProperty _price = new SimpleDoubleProperty(0);
    private final IntegerProperty _inStock = new SimpleIntegerProperty(0);
    private final IntegerProperty _min = new SimpleIntegerProperty(0);
    private final IntegerProperty _max = new SimpleIntegerProperty(0);
    private final ObservableList<Part> _associatedParts;

    public Product(int id, String name, double price,int stock, int min, int max, 
                   ObservableList<Part> newPart) {
        setProductId(id);
        setName(name);
        setPrice(price);
        setInStock(stock);
        setMin(min);
        setMax(max);
        
        _associatedParts = FXCollections.observableArrayList();
        for(Part _part : newPart){
            _associatedParts.add(_part);
        }
    }

    
    // ArrayList Operations
    public ObservableList<Part> getPartList(){
        return _associatedParts;
    }
    public void addAssociatedPart(Part newPart){
        _associatedParts.add(newPart);
    }
    public boolean removeAssociatedPart(int id){
        for(Part _part : _associatedParts){
            if(_part.getPartId() == id){
                return _associatedParts.remove(_part);
            }
        }
         return false;
    }
    public Part lookupAssociatePart(int id){
        for(Part _part : _associatedParts){
            if(_part.getPartId() == id){
                return _part;
            }
        }
        return null;
    }
   public int getInStock() {
        return _inStock.get();
    }

    public final void setInStock(int value) {
        _inStock.set(value);
    }

    public IntegerProperty _inStockProperty() {
        return _inStock;
    }

    public int getMax() {
        return _max.get();
    }

    public final void setMax(int value) {
        _max.set(value);
    }

    public IntegerProperty _maxProperty() {
        return _max;
    }
    

    public int getMin() {
        return _min.get();
    }

    public final void setMin(int value) {
        _min.set(value);
    }

    public IntegerProperty _minProperty() {
        return _min;
    }
    

    public double getPrice() {
        return _price.get();
    }

    public final void setPrice(double value) {
        _price.set(value);
    }

    public DoubleProperty _priceProperty() {
        return _price;
    }
    

    public String getName() {
        return _name.get();
    }

    public final void setName(String value) {
        _name.set(value);
    }

    public StringProperty _nameProperty() {
        return _name;
    }
    

    public int getProductId() {
        return _productId.get();
    }

    public final void setProductId(int value) {
        _productId.set(value);
    }

    public IntegerProperty _productIdProperty() {
        return _productId;
    }
}
