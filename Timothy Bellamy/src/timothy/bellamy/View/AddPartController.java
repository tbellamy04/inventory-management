/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timothy.bellamy.View;
import timothy.bellamy.Model.*;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Timothy Bellamy
 */
public class AddPartController implements Initializable {

    static private Inventory _inventory;
    
    
    @FXML
    private Button addSave, cancel;
    @FXML
    private RadioButton inHousePart, outsourced;
    @FXML
    private TextField name, inventory, price, min, max, partTypeField;
    @FXML
    private Label partType;
    @FXML
    private ToggleGroup toggleGroup1;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }  

    @FXML
    private void onRadioSelect(ActionEvent event)throws IOException{
        
        RadioButton selectedButton = (RadioButton) toggleGroup1.getSelectedToggle();
        
        if(selectedButton==inHousePart){
            partType.setText("Machine ID");
            partTypeField.setPromptText("Machine ID");

        }else{
            partType.setText("Company Name");
            partTypeField.setPromptText("Company Name");
        }


    }
    @FXML
    private void onSavePart(ActionEvent event)throws IOException{
        
        int id = _inventory.getUniquePartId();
        String pName = name.getText();
        int partAmount = Integer.parseInt(inventory.getText());
        double pPrice = Double.parseDouble(price.getText());
        int minPart = Integer.parseInt(min.getText());
        int maxPart = Integer.parseInt(max.getText());
        
        RadioButton selectedButton = (RadioButton) toggleGroup1.getSelectedToggle();
        if(selectedButton == inHousePart){
            int machine = Integer.parseInt(partTypeField.getText());
            Part newPart = new InHouse(id, pName, pPrice, partAmount, 
                                       minPart, maxPart, machine);
            _inventory.addPart(newPart);
        }else{
            String companyName = partTypeField.getText();
            Part newPart = new Outsourced(id, pName, pPrice, partAmount, 
                                                minPart, maxPart, companyName);
            _inventory.addPart(newPart);
        }
        
        // Update Part Id
        _inventory.setUniquePartId((id+1));

        
        Stage stage;
        Parent root;
        stage = (Stage) addSave.getScene().getWindow();
        
        FXMLLoader loader = new FXMLLoader();
        root = FXMLLoader.load(getClass().getResource("mainScreen.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    @FXML
    private void onCancel(ActionEvent event)throws IOException{
 
        ButtonType yes = new ButtonType("Yes", ButtonBar.ButtonData.OK_DONE);
        ButtonType no = new ButtonType("No", ButtonBar.ButtonData.CANCEL_CLOSE);
        
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                        "Do you want to cancel creating current part",
                        yes, no);
        alert.setTitle("Confirmation");
        alert.setHeaderText("");


        Optional<ButtonType> result = alert.showAndWait();
        
        if(result.get() == yes){
                Stage stage;
                Parent root;
                stage = (Stage) cancel.getScene().getWindow();

                FXMLLoader loader = new FXMLLoader();
                root = FXMLLoader.load(getClass().getResource("mainScreen.fxml"));
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
          }
    }
    
    public void setInventory(Inventory inv){
          AddPartController._inventory = inv;
        }
}
    
