/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timothy.bellamy.View;
import java.io.IOException;
import java.util.Observable;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import timothy.bellamy.Model.*;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Timothy Bellamy
 */
public class AddProductController{
    private static Inventory _inventory;
    private  static ObservableList<Part> aParts = FXCollections.observableArrayList();
    
    @FXML
    private TableView<Part> partTable, associatedPartTable;
    @FXML
    private TableColumn<Part,Integer> partId, aPartId;
    @FXML
    private TableColumn<Part,String> partName, aPartName;
    @FXML
    private TableColumn<Part,Integer> partStock, aPartStock;
    @FXML
    private TableColumn<Part,Double> partPrice, aPartPrice;
    @FXML
    private Button save, cancel;
    @FXML
    private TextField name, inventory, price, min, max, partSearchField;
    /**
     * Initializes the controller class.
     */
    @FXML
    public void initialize() {} 
    @FXML
    private void onCancel(ActionEvent event)throws IOException{
        ButtonType yes = new ButtonType("Yes", ButtonBar.ButtonData.OK_DONE);
        ButtonType no = new ButtonType("No", ButtonBar.ButtonData.CANCEL_CLOSE);
        
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                        "Do you want to cancel creating current product",
                        yes, no);
        alert.setTitle("Confirmation");
        alert.setHeaderText("");


        Optional<ButtonType> result = alert.showAndWait();
        
        if(result.get() == yes){
                Stage stage;
                Parent root;
                stage = (Stage) cancel.getScene().getWindow();

                FXMLLoader loader = new FXMLLoader();
                root = FXMLLoader.load(getClass().getResource("mainScreen.fxml"));
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
          }
    }
    @FXML
    private void onAddPart(ActionEvent event)throws IOException{
        Part selectedPart = partTable.getSelectionModel().getSelectedItem();
        
        aParts.add(selectedPart);
        
            aPartId.setCellValueFactory(cellData -> 
                    cellData.getValue()._partIdProperty().asObject());
            aPartName.setCellValueFactory(cellData ->
                    cellData.getValue()._nameProperty());
            aPartStock.setCellValueFactory(cellData ->
                    cellData.getValue()._inStockProperty().asObject());
            aPartPrice.setCellValueFactory(cellData ->
                    cellData.getValue()._priceProperty().asObject());
            associatedPartTable.setItems(aParts);        
    }
    @FXML
    private void onRemovePart(ActionEvent event)throws IOException{
        Part selectedPart = associatedPartTable.getSelectionModel().getSelectedItem();
        aParts.remove(selectedPart);
    }
    @FXML
    private void onSave(ActionEvent event)throws IOException{
        
        if(aParts.isEmpty()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("");
            alert.setContentText("All products must have atleast one associated part");
            alert.showAndWait();
        }else{
            int productId = _inventory.getUniqueProductId();
            String productName = name.getText();
            int productStock = Integer.parseInt(inventory.getText());
            double productPrice = Double.parseDouble(price.getText());
            int productMin = Integer.parseInt(min.getText());
            int productMax = Integer.parseInt(max.getText());

            Product newProduct = new Product(productId, productName, productPrice, 
                                       productStock, productMin, productMax, aParts);
            _inventory.addProduct(newProduct);

            Stage stage;
            Parent root;

            stage = (Stage) save.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader();
            root = FXMLLoader.load(getClass().getResource("mainScreen.fxml"));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();   
        }
    }
    @FXML
    private void onPartSearch(ActionEvent event)throws IOException{
        int id = Integer.parseInt(partSearchField.getText());
        if(_inventory.lookupPart(id) != null){
        partTable.getItems().stream().filter(part -> part.getPartId() == id)
                 .findAny()
                 .ifPresent((Part part) -> {
                     partTable.getSelectionModel().select(part);
                     partTable.scrollTo(part);});
        }else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Search Information");
            alert.setHeaderText("");
            alert.setContentText("Part ID " + id + " not found");
            alert.showAndWait();
        }
    }
    public void setInventory(Inventory inv){
        AddProductController._inventory = inv;
        loadTables();
    }
    @FXML
    private void loadTables(){
            partId.setCellValueFactory(cellData -> 
                    cellData.getValue()._partIdProperty().asObject());
            partName.setCellValueFactory(cellData ->
                    cellData.getValue()._nameProperty());
            partStock.setCellValueFactory(cellData ->
                    cellData.getValue()._inStockProperty().asObject());
            partPrice.setCellValueFactory(cellData ->
                    cellData.getValue()._priceProperty().asObject());
            partTable.setItems(_inventory.allParts);
    }
}
