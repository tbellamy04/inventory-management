/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timothy.bellamy.View;
import java.io.IOException;
import java.util.Optional;
import java.util.function.Consumer;
import timothy.bellamy.Model.*;

import static javafx.application.Platform.exit;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Timothy Bellamy
 */
public class MainScreenController{
    
    // Initializes inventory
    private static final Inventory _inventory = new Inventory();

    @FXML
    private TableView<Part> partTable;
    @FXML
    private TableView<Product> productTable;
    @FXML
    private TableColumn<Part,Integer> partId;
    @FXML
    private TableColumn<Part,String> partName;
    @FXML
    private TableColumn<Part,Integer> partStock;
    @FXML
    private TableColumn<Part,Double> partPrice;
    @FXML
    private TableColumn<Product,Integer> productId;
    @FXML
    private TableColumn<Product,String> productName;
    @FXML
    private TableColumn<Product,Integer> productStock;
    @FXML
    private TableColumn<Product,Double> productPrice;
    @FXML
    private Button addPart, modifyPart, addProduct, modifyProduct;
    @FXML
    private TextField partSearchField, productSearchField;
    /**
     * Initializes the controller class.
     */
    @FXML
    public void initialize() {
        
        partId.setCellValueFactory(cellData -> 
                cellData.getValue()._partIdProperty().asObject());
        partName.setCellValueFactory(cellData ->
                cellData.getValue()._nameProperty());
        partStock.setCellValueFactory(cellData ->
                cellData.getValue()._inStockProperty().asObject());
        partPrice.setCellValueFactory(cellData ->
                cellData.getValue()._priceProperty().asObject());
        partTable.setItems(_inventory.allParts);
      
        
        productId.setCellValueFactory(cellData ->
                cellData.getValue()._productIdProperty().asObject());
        productName.setCellValueFactory(cellData ->
                cellData.getValue()._nameProperty());
        productStock.setCellValueFactory(cellData ->
                cellData.getValue()._inStockProperty().asObject());
        productPrice.setCellValueFactory(cellData ->
                cellData.getValue()._priceProperty().asObject());
        productTable.setItems(_inventory.products);
   
    }
    // Main Screen Part Controlls
    @FXML
    private void onClickAddPart(ActionEvent event)throws IOException{
        Stage stage;
        Parent root;
        
        stage=(Stage) addPart.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("addPart.fxml"));
        
        root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        AddPartController controller = loader.getController();
        controller.setInventory(_inventory);
        
    }
    @FXML
    private void onClickModify(ActionEvent event)throws IOException{
        Part selectedPart = partTable.getSelectionModel().getSelectedItem();
                
        if(selectedPart == null){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Invlaid Part");
            alert.setHeaderText("Invalid Part");
            alert.setContentText("Please seleect a part to modify");
            alert.showAndWait();
        }else{
            Stage stage;
            Parent root;
            FXMLLoader loader;
            stage = (Stage) modifyPart.getScene().getWindow();
            loader = new FXMLLoader(getClass().getResource("modifyPart.fxml"));
            root = loader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();

            ModifyPartController controller = loader.getController();
            controller.setInventory(_inventory);
            controller.setPart(selectedPart);
        }
    }
    @FXML
    private void onClickExit(){
        exit();
    }
    @FXML
    private void onClickDeletePart(){
       Part selectedpart = partTable.getSelectionModel().getSelectedItem();
       
       if(selectedpart != null ){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation");
            alert.setHeaderText("Confirmation");
            alert.setContentText("Deleting part " + selectedpart.getName());
            Optional<ButtonType> result = alert.showAndWait();
            if(result.get() == ButtonType.OK){
                _inventory.deletePart(selectedpart.getPartId());
            }
       }else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Invlaid Part");
            alert.setHeaderText("Invalid Part");
            alert.setContentText("Please select a part to delete");
            alert.showAndWait();
       }
    }
    @FXML
    private void onPartSearch(ActionEvent event)throws IOException{
        int id = Integer.parseInt(partSearchField.getText());
        if(_inventory.lookupPart(id) != null){
        partTable.getItems().stream().filter(part -> part.getPartId() == id)
                 .findAny()
                 .ifPresent((Part part) -> {
                     partTable.getSelectionModel().select(part);
                     partTable.scrollTo(part);});
        }else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Search Information");
            alert.setHeaderText("");
            alert.setContentText("Part ID " + id + " not found");
            alert.showAndWait();
        }
    }
    
    // Main Screen Product Controlls
    @FXML
    private void onClickDeleteProduct(){
        Product selectedProduct = productTable.getSelectionModel().getSelectedItem();
       if(selectedProduct != null ){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation");
            alert.setHeaderText("Confirmation");
            alert.setContentText("Deleting product " + selectedProduct.getName());
            Optional<ButtonType> result = alert.showAndWait();
            if(result.get() == ButtonType.OK){
                _inventory.removeProduct(selectedProduct.getProductId());
            }
       }else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Invlaid Product");
            alert.setHeaderText("Invalid product");
            alert.setContentText("Please select a product to delete");
            alert.showAndWait();
       }
    }
    @FXML
    private void onAddProduct(ActionEvent event)throws IOException{
        Stage stage;
        Parent root;
        
        stage = (Stage) addProduct.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("addProduct.fxml"));
        
        root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        AddProductController controller = loader.getController();
        controller.setInventory(_inventory);
    }
    @FXML
    private void onModifyProduct(ActionEvent event)throws IOException{
        Product selectedProduct = productTable.getSelectionModel().getSelectedItem();
        
        if(selectedProduct == null){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Invlaid Product");
            alert.setHeaderText("Invalid Product");
            alert.setContentText("Please seleect a product to modify");
            alert.showAndWait();           
        }else{
            Stage stage;
            Parent root;

            stage = (Stage) modifyProduct.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("modifyProduct.fxml"));

            root = loader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();

            ModifyProductController controller = loader.getController();
            controller.setInventory(_inventory);
            controller.setProduct(selectedProduct);
        }
    }
    @FXML
    private void onProductSearch(ActionEvent event)throws IOException{
        int searchId = Integer.parseInt(productSearchField.getText());
        
        if(_inventory.lookupProduct(searchId) != null){
        productTable.getItems().stream().filter(product -> product.getProductId() == searchId)
                 .findAny()
                 .ifPresent((Product product) -> {
                     productTable.getSelectionModel().select(product);
                     productTable.scrollTo(product);});
        }else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Search Information");
            alert.setHeaderText("");
            alert.setContentText("Product ID " + searchId + " not found");
            alert.showAndWait();
        }      
    }
}