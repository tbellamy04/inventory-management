/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timothy.bellamy.View;
import timothy.bellamy.Model.*;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;


/**
 * FXML Controller class
 *
 * @author Timothy Bellamy
 */
public class ModifyPartController implements Initializable {

    private static Inventory _inventory;
    private static Part part;
    
    @FXML
    private Button modifySave, cancel;
    @FXML
    private RadioButton inHouse, outsourced;
    @FXML
    private TextField id, name, inventory, price, min, max, partTypeField;
    @FXML
    private Label partType;
    @FXML
    private ToggleGroup toggleGroup1;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    } 
    @FXML
    private void onRadioSelect(ActionEvent event)throws IOException{
        
        RadioButton selectedButton = (RadioButton) toggleGroup1.getSelectedToggle();
        
        if(selectedButton==inHouse){
            partType.setText("Machine ID");
            partTypeField.setPromptText("New Machine ID");
            partTypeField.setText("");
        }else{
            partType.setText("Company Name");
            partTypeField.setPromptText("New Company Name");
            partTypeField.setText("");
        }
    }
    @FXML
    private void onCancel(ActionEvent event)throws IOException{
        ButtonType yes = new ButtonType("Yes", ButtonBar.ButtonData.OK_DONE);
        ButtonType no = new ButtonType("No", ButtonBar.ButtonData.CANCEL_CLOSE);
        
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                        "Do you want to cancel modifying current part",
                        yes, no);
        alert.setTitle("Confirmation");
        alert.setHeaderText("");


        Optional<ButtonType> result = alert.showAndWait();
        
        if(result.get() == yes){
                Stage stage;
                Parent root;
                stage = (Stage) cancel.getScene().getWindow();

                FXMLLoader loader = new FXMLLoader();
                root = FXMLLoader.load(getClass().getResource("mainScreen.fxml"));
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
          }
    }
    @FXML
    private void onSave(ActionEvent event)throws IOException{
        RadioButton selectedButton = (RadioButton) toggleGroup1.getSelectedToggle();
        
        if((selectedButton == inHouse)&&(part instanceof InHouse)){
            part.setName(name.getText());
            part.setInStock(Integer.parseInt(inventory.getText()));
            part.setPrice(Double.parseDouble(price.getText()));
            part.setMin(Integer.parseInt(min.getText()));
            part.setMax(Integer.parseInt(max.getText()));
            ((InHouse) part).setMachineId(Integer.parseInt(partTypeField.getText()));
            
        }else if ((selectedButton == outsourced) && (part instanceof Outsourced)){
            part.setName(name.getText());
            part.setInStock(Integer.parseInt(inventory.getText()));
            part.setPrice(Double.parseDouble(price.getText()));
            part.setMin(Integer.parseInt(min.getText()));
            part.setMax(Integer.parseInt(max.getText()));
            ((Outsourced) part).setCompanyName(partTypeField.getText());    
        }else if((selectedButton == outsourced)&&(part instanceof InHouse)){
            int partId = Integer.parseInt(id.getText());
            String pName = name.getText();
            int partAmount = Integer.parseInt(inventory.getText());
            double pPrice = Double.parseDouble(price.getText());
            int minPart = Integer.parseInt(min.getText());
            int maxPart = Integer.parseInt(max.getText());
            String compnay = partTypeField.getText();
            
            _inventory.deletePart(partId);
            Part newpart = new Outsourced(partId, pName, pPrice, partAmount,
                                        minPart, maxPart, compnay);
            _inventory.addPart(newpart);       
        }else if((selectedButton == inHouse) && (part instanceof Outsourced)){
            int partId = Integer.parseInt(id.getText());
            String pName = name.getText();
            int partAmount = Integer.parseInt(inventory.getText());
            double pPrice = Double.parseDouble(price.getText());
            int minPart = Integer.parseInt(min.getText());
            int maxPart = Integer.parseInt(max.getText());
            int machine = Integer.parseInt(partTypeField.getText());
            
            _inventory.deletePart(partId);
            Part newPart = new InHouse(partId, pName, pPrice, partAmount, 
                                       minPart, maxPart, machine);
            _inventory.addPart(newPart);
        }
        Stage stage;
        Parent root;
        stage = (Stage) cancel.getScene().getWindow();
        
        FXMLLoader loader = new FXMLLoader();
        root = FXMLLoader.load(getClass().getResource("mainScreen.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();     
    }
    
    // Internal funcitons
    // setIntventory loads _inventory from mainScreen 
    public void setInventory(Inventory inv){
        ModifyPartController._inventory = inv;
    }
    // setPart passes selected part from mainScren
    public void setPart(Part part){
        ModifyPartController.part = part;
              
        id.setText(Integer.toString(part.getPartId()));
        name.setText(part.getName());
        inventory.setText(Integer.toString(part.getInStock()));
        price.setText(Double.toString(part.getPrice()));
        min.setText(Integer.toString(part.getMin()));
        max.setText(Integer.toString(part.getMax()));
        
        if(part instanceof InHouse){
            inHouse.setSelected(true);
            partTypeField.setText(Integer.toString(((InHouse) part).getMachineId()));
        }else{
            outsourced.setSelected(true);
            partType.setText("Company Name");
            partTypeField.setText(((Outsourced) part).getCompanyName());
        }     
    }   
}
