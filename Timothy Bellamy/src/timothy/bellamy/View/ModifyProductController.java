/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timothy.bellamy.View;
import java.io.IOException;
import timothy.bellamy.Model.*;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Timothy Bellamy
 */
public class ModifyProductController implements Initializable {

    private static Inventory _inventory;
    private static Product _product;
    
    @FXML
    private TableView<Part> partTable, associatedPartTable;
    @FXML
    private TableColumn<Part,Integer> partId, aPartId;
    @FXML
    private TableColumn<Part,String> partName, aPartName;
    @FXML
    private TableColumn<Part,Integer> partStock, aPartStock;
    @FXML
    private TableColumn<Part,Double> partPrice, aPartPrice;
    @FXML
    private Button save, cancel;
    @FXML
    private TextField id, name, inventory, price, min, max, partSearchField;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }   
    @FXML
    private void onCancel(ActionEvent event)throws IOException{
        ButtonType yes = new ButtonType("Yes", ButtonBar.ButtonData.OK_DONE);
        ButtonType no = new ButtonType("No", ButtonBar.ButtonData.CANCEL_CLOSE);
        
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                        "Do you want to cancel modifying current product",
                        yes, no);
        alert.setTitle("Confirmation");
        alert.setHeaderText("");


        Optional<ButtonType> result = alert.showAndWait();
        
        if(result.get() == yes){
                Stage stage;
                Parent root;
                stage = (Stage) cancel.getScene().getWindow();

                FXMLLoader loader = new FXMLLoader();
                root = FXMLLoader.load(getClass().getResource("mainScreen.fxml"));
                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
          }
    }
    @FXML
    private void onSave(ActionEvent event)throws IOException{
        
        if(_product.getPartList().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("");
            alert.setContentText("All products must have atleast one associated part");
            alert.showAndWait();
        }else{
            _product.setName(name.getText());
            _product.setInStock(Integer.parseInt(inventory.getText()));
            _product.setPrice(Double.parseDouble(price.getText()));
            _product.setMin(Integer.parseInt(min.getText()));
            _product.setMax(Integer.parseInt(max.getText()));


            Stage stage;
            Parent root;
            stage = (Stage) save.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader();
            root = FXMLLoader.load(getClass().getResource("mainScreen.fxml"));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();   
        }     
    }
    @FXML
    private void onAddPart(ActionEvent event)throws IOException{
        Part selectedPart = partTable.getSelectionModel().getSelectedItem();
        _product.addAssociatedPart(selectedPart);
    }
    @FXML
    private void onRemovePart(ActionEvent event)throws IOException{
        Part selectedPart = associatedPartTable.getSelectionModel().getSelectedItem();
        _product.removeAssociatedPart(selectedPart.getPartId());
    }
    @FXML
    private void onPartSearch(ActionEvent event)throws IOException{
        int searchId = Integer.parseInt(partSearchField.getText());
        if(_inventory.lookupPart(searchId) != null){
        partTable.getItems().stream().filter((Part part) -> part.getPartId() == searchId)
                 .findAny()
                 .ifPresent((Part part) -> {
                     partTable.getSelectionModel().select(part);
                     partTable.scrollTo(part);});
        }else{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Search Information");
            alert.setHeaderText("");
            alert.setContentText("Part ID " + searchId + " not found");
            alert.showAndWait();
        }
    }
    public void setInventory(Inventory inv){
        ModifyProductController._inventory = inv;
    }
    public void setProduct(Product product){
        ModifyProductController._product = product;
        
        // Set all Text Fields
        id.setText(Integer.toString(_product.getProductId()));
        name.setText(_product.getName());
        inventory.setText(Integer.toString(_product.getInStock()));
        price.setText(Double.toString(_product.getPrice()));
        min.setText(Integer.toString(_product.getMin()));
        max.setText(Integer.toString(_product.getMax()));
        
        // Populate Available Parts Table
        partId.setCellValueFactory(cellData -> 
               cellData.getValue()._partIdProperty().asObject());
        partName.setCellValueFactory(cellData ->
                 cellData.getValue()._nameProperty());
        partStock.setCellValueFactory(cellData ->
                  cellData.getValue()._inStockProperty().asObject());
        partPrice.setCellValueFactory(cellData ->
                  cellData.getValue()._priceProperty().asObject());
        partTable.setItems(_inventory.allParts);   
        
        // Populate Asssociated Parts Table        
        aPartId.setCellValueFactory(cellData -> 
                cellData.getValue()._partIdProperty().asObject());
        aPartName.setCellValueFactory(cellData ->
                  cellData.getValue()._nameProperty());
        aPartStock.setCellValueFactory(cellData ->
                   cellData.getValue()._inStockProperty().asObject());
        aPartPrice.setCellValueFactory(cellData ->
                   cellData.getValue()._priceProperty().asObject());
        associatedPartTable.setItems(_product.getPartList()); 
        
    }
}
